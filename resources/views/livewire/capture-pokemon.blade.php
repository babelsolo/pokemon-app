<section class="page-section bg-primary text-white mb-0" id="about">
    <div class="container">
        <!-- About Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-white">Capture Pokémon</h2>
        <!-- Icon Divider-->
        <div class="divider-custom divider-light">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon">
                <i class="fas fa-bullseye"></i>
            </div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- About Section Content-->
        <div class="row justify-content-center">
            <div class="col-md-3 col-lg-3 mb-5">
                <div class="mx-auto"
                    data-id="{{ $pokemon['id'] ?? '' }}"
                    data-name="{{ $pokemon['name'] ?? '' }}"
                    data-image="{{ $pokemon['sprites']['front_default'] ?? '' }}">
                    <div class="d-flex align-items-center justify-content-center h-100 w-100">
                        <div class="text-center text-white"></div>
                    </div>
                    <figure>
                        <img class="img-fluid mx-auto d-block" src="{{ $pokemon['sprites']['front_default'] ?? '' }}"
                            alt="{{ $pokemon['name'] ?? '' }}" width="80%" />
                        <figcaption class="text-center text-primary text-white fs-1">{{ $pokemon['name'] ?? '' }}
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
        <!-- About Section Button-->
        <div class="text-center mt-4">
            <a href="javascript:void(0)" class="btn btn-xl btn-outline-light btn-capture-pokemon" data-id="{{ $pokemon['id'] ?? '' }}">
                <img src="{{ asset('img/pokeball.png') }}" width="15%">
                Capture!
            </a>
        </div>
    </div>
</section>
