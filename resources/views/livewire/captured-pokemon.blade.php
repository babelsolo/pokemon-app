<section class="page-section">
    <div class="container">
        <!-- Portfolio Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Captured Pokémons</h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon">
                <i class="fas fa-bullseye"></i>
            </div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Portfolio Grid Items-->
        <div class="row justify-content-center">

            @foreach ($pokemons as $pokemon)
                <x-captured-pokemon id="{{ $pokemon->id }}" name="{{ $pokemon->name }}" image="{{ $pokemon->image }}" />
            @endforeach

        </div>
    </div>
</section>
