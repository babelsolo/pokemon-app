<header class="masthead bg-secondary text-white text-center">
    <div class="container d-flex align-items-center flex-column">
        <!-- Masthead Avatar Image-->
        <img class="masthead-avatar mb-5" src="{{ asset('img/oak.png') }}" alt="..." width="20%" />
        <!-- Masthead Heading-->
        <h1 class="masthead-heading text-uppercase mb-0">Pokémon App</h1>
        <!-- Icon Divider-->
        <div class="divider-custom divider-light">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Masthead Subheading-->
        <p class="masthead-subheading font-weight-light mb-0">
            The early bird catches the worm, or in this case gets its Pokémon.
        </p>
    </div>
</header>
