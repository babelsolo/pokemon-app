<div class="col-2">
    <div class="mx-auto"
        data-id="{{ $id ?? '' }}"
        data-name="{{ $name ?? '' }}"
        data-image="{{ $image ?? '' }}">
        <div class="d-flex align-items-center justify-content-center h-100 w-100">
            <div class="text-center text-white"></div>
        </div>
        <figure>
            <img class="img-fluid mx-auto d-block" src="{{ $image ?? '' }}" alt="{{ $name ?? '' }}" width="100%" />

            <div class="d-flex align-items-center justify-content-center h-100 w-100">
                <button class="btn btn-primary ms-2 btn-evolve-pokemon" data-id="{{ $id ?? '' }}" title="evolve">
                    <i class="fas fa-fire fa-1x"></i>
                </button>
                <button class="btn btn-danger ms-2 btn-leave-pokemon" data-id="{{ $id ?? '' }}" title="leave">
                    <i class="fas fa-sign-out-alt fa-1x"></i>
                </button>
            </div>

            <figcaption class="text-center text-primary fs-3">{{ $name ?? '' }}</figcaption>
        </figure>
    </div>
</div>
