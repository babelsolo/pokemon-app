<div class="col-md-3 col-lg-3 mb-5">
    <div class="portfolio-item mx-auto"
        data-id="{{ $id ?? '' }}"
        data-name="{{ $name ?? '' }}"
        data-image="{{ $image ?? '' }}">
        <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
            <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
        </div>
        <figure>
            <img class="img-fluid mx-auto d-block" src="{{ $image ?? '' }}" alt="{{ $name ?? '' }}" width="80%" />
            <figcaption class="text-center text-primary fs-3">{{ $name ?? '' }}</figcaption>
        </figure>
    </div>
</div>
