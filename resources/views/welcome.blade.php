<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Pokemón App</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Styles -->

    <style>
        body {
            font-family: 'Nunito';
            background: #f7fafc;
        }
    </style>

    @vite(['resources/sass/app.scss', 'resources/css/app.css', 'resources/js/app.js'])

    @livewireStyles
</head>

<body id="page-top">

    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg bg-{{ auth()->check() ? 'secondary' : 'primary' }} text-uppercase fixed-top"
        id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="#page-top">Pokémon App</a>
            <button class="navbar-toggler text-uppercase font-weight-bold bg-primary text-white rounded" type="button"
                data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive"
                aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ms-auto">

                    @if (auth()->check())
                        <li class="nav-item mx-0 mx-lg-1">
                            <!-- Authentication -->
                            <x-jet-dropdown-link class="nav-link py-3 px-0 px-lg-3 rounded" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                {{ __('Log out') }}
                            </x-jet-dropdown-link>
                            <form method="POST" id="logout-form" action="{{ route('logout') }}">
                                @csrf
                            </form>
                        </li>
                    @endif

                </ul>
            </div>
        </div>
    </nav>

    @auth
        <x-auth-header />
    @else
        <x-header />
    @endauth

    <!-- pokemon-available -->
    @if (!auth()?->user()?->first_pokemon)
        @include('layouts.pokemon-available')
    @else
        @livewire('captured-pokemon')
    @endif

    <!-- About Section-->
    @if (auth()->check() && auth()->user()->first_pokemon)
        @livewire('capture-pokemon')
    @endif

    @if (!auth()->check())
        @include('layouts.login-register')
    @endif

    <!-- Footer-->
    @include('layouts.footer')
    <!-- Copyright Section-->

    <div class="copyright py-4 text-center text-white">
        <div class="container"><small>Copyright &copy; Pokémon App 2023</small></div>
    </div>
    <!-- Portfolio Modals-->
    <!-- Portfolio Modal 1-->
    @include('layouts.modal')

    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>


    @livewireScripts

    @vite(['resources/js/user-pokemon/main.js'])

</body>

</html>
