<section class="page-section bg-primary text-white" id="contact">
    <div class="container">
        <!-- Contact Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-white mb-0">
            Start your pokemon training
        </h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
        </div>
        <!-- Contact Section Form-->
        <div class="row justify-content-center">
            <div class="col-md-5 col-lg-5 col-xl-5">

                <h5 class="text-center text-uppercase text-white mb-0">Log In</h5>

                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="mb-3">
                        <x-jet-label value="{{ __('Email') }}" />

                        <x-jet-input class="{{ $errors->has('email') ? 'is-invalid' : '' }}" type="email"
                            name="email" :value="old('email')" required />
                        <x-jet-input-error for="email"></x-jet-input-error>
                    </div>

                    <div class="mb-3">
                        <x-jet-label value="{{ __('Password') }}" />

                        <x-jet-input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                            type="password" name="password" required autocomplete="current-password" />
                        <x-jet-input-error for="password"></x-jet-input-error>
                    </div>

                    <div class="mb-3">
                        <div class="custom-control custom-checkbox">
                            <x-jet-checkbox id="remember_me" name="remember" />
                            <label class="custom-control-label" for="remember_me">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>

                    <div class="mb-0">
                        <div class="d-flex justify-content-end align-items-baseline">
                            <x-jet-button>
                                {{ __('Log in') }}
                            </x-jet-button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-2 col-lg-2 col-xl-2"></div>

            <div class="col-md-5 col-lg-5 col-xl-5">

                <h5 class="text-center text-uppercase text-white mb-0">Register</h5>

                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="mb-3">
                        <x-jet-label value="{{ __('Name') }}" />

                        <x-jet-input class="{{ $errors->has('name') ? 'is-invalid' : '' }}" type="text"
                            name="name" :value="old('name')" required autofocus autocomplete="name" />
                        <x-jet-input-error for="name"></x-jet-input-error>
                    </div>

                    <div class="mb-3">
                        <x-jet-label value="{{ __('Email') }}" />

                        <x-jet-input class="{{ $errors->has('email') ? 'is-invalid' : '' }}" type="email"
                            name="email" :value="old('email')" required />
                        <x-jet-input-error for="email"></x-jet-input-error>
                    </div>

                    <div class="mb-3">
                        <x-jet-label value="{{ __('Password') }}" />

                        <x-jet-input class="{{ $errors->has('password') ? 'is-invalid' : '' }}"
                            type="password" name="password" required autocomplete="new-password" />
                        <x-jet-input-error for="password"></x-jet-input-error>
                    </div>

                    <div class="mb-3">
                        <x-jet-label value="{{ __('Confirm Password') }}" />

                        <x-jet-input class="form-control" type="password" name="password_confirmation"
                            required autocomplete="new-password" />
                    </div>

                    <div class="mb-0">
                        <div class="d-flex justify-content-end align-items-baseline">
                            <x-jet-button>
                                {{ __('Register') }}
                            </x-jet-button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
