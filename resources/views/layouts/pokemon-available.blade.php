<section class="page-section portfolio" id="portfolio">
    <div class="container">
        <!-- Portfolio Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Pokémons Available</h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Portfolio Grid Items-->
        <div class="row justify-content-center">

            <x-pokemon-available id="{{ $bulbasaur['id'] }}" name="{{ $bulbasaur['name'] }}" image="{{ $bulbasaur['sprites']['front_default'] }}" />
            <x-pokemon-available id="{{ $charmander['id'] }}" name="{{ $charmander['name'] }}" image="{{ $charmander['sprites']['front_default'] }}" />
            <x-pokemon-available id="{{ $squirtle['id'] }}" name="{{ $squirtle['name'] }}" image="{{ $squirtle['sprites']['front_default'] }}" />
            <x-pokemon-available id="{{ $pikachu['id'] }}" name="{{ $pikachu['name'] }}" image="{{ $pikachu['sprites']['front_default'] }}" />
            <x-pokemon-available id="{{ $eevee['id'] }}" name="{{ $eevee['name'] }}" image="{{ $eevee['sprites']['front_default'] }}" />
        </div>
    </div>
</section>
