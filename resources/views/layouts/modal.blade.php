<div class="portfolio-modal modal fade" id="modalFormPokemon" tabindex="-1" aria-labelledby="portfolioModal1"
    aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center pb-5">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <!-- Portfolio Modal - Title-->
                            <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0">
                                <span id="pokemon-name"></span>
                            </h2>
                            <!-- Icon Divider-->
                            <div class="divider-custom">
                                <div class="divider-custom-line"></div>
                                <div class="divider-custom-icon"><i class="fas fa-bullseye"></i></div>
                                <div class="divider-custom-line"></div>
                            </div>
                            <!-- Portfolio Modal - Image-->
                            <img class="img-fluid rounded" id="pokemon-image" />

                            @if (!auth()->check())
                                <p class="fs-1">
                                    To select your first pokémon, you must register or log in.
                                </p>
                            @endif

                            <button class="btn btn-primary" data-bs-dismiss="modal">
                                <i class="fas fa-xmark fa-fw"></i>
                                Close Window
                            </button>

                            @auth
                                <button id="btn-my-first-pokemon" class="btn btn-primary btn-my-first-pokemon">
                                    <i class="fas fa-xmark fa-fw"></i>
                                    Select Pokemon
                                </button>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
