const form = async (url, data) => {
    // startLoading();

    await axios({
        method: 'POST',
        url: url,
        data: data,
    })
        .then((response) => {
            Swal.fire(response?.data?.title, response?.data?.message, "success");
            $(`#modalFormPokemon`).modal("hide");
        })
        .catch((error) => {
            let errors = error?.response?.data?.errors;
            let str_error = '<ul class="list-unstyled">';

            $.each(errors, function (index, error) {
                str_error += `
                        <ol class="text-start">
                            <i class="fas fa-angle-double-right"></i>
                            ${error}
                        </ol>`;
            });

            str_error += `</ul>`;

            Swal.fire({
                icon: "error",
                html: str_error,
                confirmButtonText: "Volver",
            });
        })
        .then(function () {
            // finishLoading();
        });
};

export default form;
