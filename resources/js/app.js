import "./bootstrap";

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();

/**
 * CSS
 */
import "../assets/css/styles.css"

/**
 * JS
 */
import "../assets/js/scripts.js";
