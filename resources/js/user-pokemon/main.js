$(document).on("click", ".btn-select-pokemon", function () {
    let id = $(this).data("id");
    let name = $(this).data("name");
    let image = $(this).data("image");

    $("#btn-my-first-pokemon").attr("data-id", id);
    $("#pokemon-name").html(name);

    $("#pokemon-image").attr("src", image).attr("width", "80%");

    $("#modalFormPokemon").modal("show");
});

$(document).on("click", ".btn-my-first-pokemon", function () {
    const pokemon_id = $(this).data("id");
    capturePokemon(pokemon_id);
});

$(document).on("click", ".btn-capture-pokemon", function () {
    const pokemon_id = $(this).data("id");
    capturePokemon(pokemon_id);
});

$(document).on("click", ".btn-evolve-pokemon", function () {
    const pokemon_id = $(this).data("id");
    evolvePokemon(pokemon_id);
});

$(document).on("click", ".btn-leave-pokemon", function () {
    const pokemon_id = $(this).data("id");
    leavePokemon(pokemon_id);
});

const capturePokemon = async (pokemon_id) => {

    const url = "/user-pokemon";

    const data = {
        pokemon_id: pokemon_id,
    };

    await axios({
        method: 'POST',
        url: url,
        data: data,
    })
        .then((response) => {
            Swal.fire(response?.data?.title, response?.data?.message, "success");

            setTimeout(() => {
                location.reload()
            }, 500);
        })
        .catch((error) => {
            let errors = error?.response?.data?.errors;
            let str_error = '<ul class="list-unstyled">';

            $.each(errors, function (index, error) {
                str_error += `
                        <ol class="text-start">
                            <i class="fas fa-angle-double-right"></i>
                            ${error}
                        </ol>`;
            });

            str_error += `</ul>`;

            Swal.fire({
                icon: "error",
                html: str_error,
                confirmButtonText: "Volver",
            });
        })
        .then(function () {
            // finishLoading();
        });
}

const evolvePokemon = async (pokemon_id) => {

    const url = `/user-pokemon/${pokemon_id}`;

    await axios({
        method: 'PUT',
        url: url
    })
        .then((response) => {
            Swal.fire(response?.data?.title, response?.data?.message, "success");

            setTimeout(() => {
                location.reload()
            }, 500);
        })
        .catch((error) => {

            Swal.fire({
                icon: "error",
                html: error?.response?.data?.message,
                confirmButtonText: "Volver",
            });
        })
        .then(function () {
            // finishLoading();
        });
}

const leavePokemon = async (pokemon_id) => {

    const url = `/user-pokemon/${pokemon_id}`;

    await axios({
        method: 'DELETE',
        url: url
    })
        .then((response) => {
            Swal.fire(response?.data?.title, response?.data?.message, "success");

            setTimeout(() => {
                location.reload()
            }, 500);
        })
        .catch((error) => {

            Swal.fire({
                icon: "error",
                html: error?.response?.data?.message,
                confirmButtonText: "Volver",
            });
        })
        .then(function () {
            // finishLoading();
        });
}
