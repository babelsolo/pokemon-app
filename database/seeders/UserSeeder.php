<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'     => 'Diego M Otero',
            'email'    => 'babelsolo@gmail.com',
            'password' => bcrypt('1234'),
        ]);
    }
}
