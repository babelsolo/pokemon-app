<?php

namespace App\Traits;

use App\Models\PokemonEvolution;

trait PokemonEvolutionTrait {

    public function setPokemonEvolution($userPokemon, $evolutions)
    {
        foreach ($evolutions as $key => $evolution) {

            $pokemonEvolution = new PokemonEvolution;

            $pokemonEvolution->name            = $evolution['name'];
            $pokemonEvolution->image           = $evolution['image'];
            $pokemonEvolution->species_id      = $evolution['species_id'];
            $pokemonEvolution->order           = $key;
            $pokemonEvolution->evolution       = 0;
            $pokemonEvolution->user_pokemon_id = $userPokemon['id'];

            $pokemonEvolution->save();
        }
    }
}
