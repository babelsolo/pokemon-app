<?php

namespace App\Traits;

use Illuminate\Support\Facades\Http;

trait PokemonTrait {

    public $evolutions;

    public function getPokemon($pokemon_id)
    {
        $pokemon         = json_decode(Http::get("https://pokeapi.co/api/v2/pokemon/{$pokemon_id}"), true);
        $species         = json_decode(Http::get($pokemon['species']['url']), true);
        $evolution_chain = json_decode(Http::get($species['evolution_chain']['url']), true);

        $evolution_chain = $evolution_chain['chain']['evolves_to'];

        foreach ($evolution_chain as $key => $evolution) {

            $next = $this->nextEvolution($evolution);

            foreach ($next as $key => $evolution) {

                $next = $this->nextEvolution($evolution);

                foreach ($next as $key => $evolution) {

                    $next = $this->nextEvolution($evolution);

                    foreach ($next as $key => $evolution) {

                        $next = $this->nextEvolution($evolution);

                        foreach ($next as $key => $evolution) {

                            $next = $this->nextEvolution($evolution);

                            foreach ($next as $key => $evolution) {

                                $next = $this->nextEvolution($evolution);

                                foreach ($next as $key => $evolution) {

                                    $next = $this->nextEvolution($evolution);

                                }
                            }
                        }
                    }
                }
            }
        }

        return [
            'id'         => $pokemon['id'],
            'name'       => $pokemon['name'],
            'image'      => $pokemon['sprites']['front_default'],
            'species_id' => $species['id'],
            'evolutions' => $this->evolutions
        ];
    }

    public function nextEvolution($evolution)
    {
        $next_pokemon  = json_decode(Http::get("https://pokeapi.co/api/v2/pokemon/{$evolution['species']['name']}"), true);
        $next_especies = json_decode(Http::get($next_pokemon['species']['url']), true);

        $this->evolutions[] = [
            'id'         => $next_pokemon['id'],
            'name'       => $next_pokemon['name'],
            'image'      => $next_pokemon['sprites']['front_default'],
            'species_id' => $next_especies['id'],
        ];

        return $evolution['evolves_to'];
    }
}
