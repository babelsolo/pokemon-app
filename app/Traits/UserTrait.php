<?php

namespace App\Traits;

use App\Models\User;
use PhpParser\Node\Expr\BinaryOp\BooleanOr;

trait UserTrait {

    public function validateUserFirstPokemon()
    {
        if (!auth()->user()->first_pokemon) {
            $user = User::find(auth()->user()->id);
            $user->first_pokemon = 1;
            $user->save();
        }
    }
}
