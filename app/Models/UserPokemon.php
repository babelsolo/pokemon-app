<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPokemon extends Model
{
    use HasFactory;

    public function pokemonEvolution()
    {
        return $this->hasMany(PokemonEvolution::class, 'user_pokemon_id')
            ->where('evolution', 0)
            ->orderBy('order', 'ASC')
            ->first();
    }

}
