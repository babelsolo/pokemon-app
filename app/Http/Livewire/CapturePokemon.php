<?php

namespace App\Http\Livewire;

use Livewire\Component;
use PokePHP\PokeApi;

class CapturePokemon extends Component
{
    public $pokemon;

    public function mount()
    {
        $api = new PokeApi;
        $this->pokemon = json_decode($api->pokemonForm(rand(1, 151)), true);
    }

    public function render()
    {
        return view('livewire.capture-pokemon');
    }
}
