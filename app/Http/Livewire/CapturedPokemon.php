<?php

namespace App\Http\Livewire;

use App\Models\UserPokemon;
use Livewire\Component;

class CapturedPokemon extends Component
{
    public $pokemons;

    public function mount()
    {
        $this->pokemons = UserPokemon::from('user_pokemon as up')
            ->where(['up.user_id' => auth()->user()->id, 'up.status' => 1])
            ->leftJoin('pokemon_evolutions as pe', function($join){
                $join->on('pe.user_pokemon_id', '=', 'up.id');
                $join->whereRaw("pe.evolution = 1");
                $join->whereRaw("pe.order = (SELECT MAX(pe2.order) FROM pokemon_evolutions pe2
                                    WHERE pe2.evolution = 1
                                    AND pe2.user_pokemon_id = pe.user_pokemon_id)");
            })
            ->selectRaw("
                CASE WHEN up.evolution = 0 THEN up.id ELSE up.id END AS id,
                CASE WHEN up.evolution = 0 THEN up.name ELSE pe.name END AS name,
                CASE WHEN up.evolution = 0 THEN up.image ELSE pe.image END AS image")
            ->get();
    }

    public function render()
    {
        return view('livewire.captured-pokemon');
    }
}
