<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PokePHP\PokeApi;

class WelcomeController extends Controller
{
    public function index()
    {
        $api = new PokeApi;

        $bulbasaur  = json_decode($api->pokemonForm(1), true);
        $charmander = json_decode($api->pokemonForm(4), true);
        $squirtle   = json_decode($api->pokemonForm(7), true);
        $pikachu    = json_decode($api->pokemonForm(25), true);
        $eevee      = json_decode($api->pokemonForm(133), true);

        return view('welcome', compact('bulbasaur', 'charmander', 'squirtle', 'pikachu', 'eevee'));
    }
}
