<?php

namespace App\Http\Controllers;

use App\Models\UserPokemon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Traits\UserTrait;
use App\Traits\PokemonTrait;
use App\Traits\PokemonEvolutionTrait;

class UserPokemonController extends Controller
{
    use UserTrait;
    use PokemonTrait;
    use PokemonEvolutionTrait;

    public function store(Request $request)
    {
        /**
         * Validate
         */
        $validator = $this->validateForm($request);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()->all(),
            ], 500);
        }

        try {

            DB::transaction(function () use ($request) {

                /**
                 * Get Pokemon
                 */
                $pokemon = $this->getPokemon($request->pokemon_id);

                /**
                 * New
                 */
                $userPokemon = new UserPokemon;

                $userPokemon->name       = $pokemon['name'];
                $userPokemon->image      = $pokemon['image'];
                $userPokemon->pokemon_id = $pokemon['id'];
                $userPokemon->species_id = $pokemon['species_id'];
                $userPokemon->evolution  = 0;
                $userPokemon->user_id    = auth()->user()->id;

                $userPokemon->save();

                /**
                 * Validate First Pokemon
                 */
                $this->validateUserFirstPokemon();

                /**
                 * Set Pokemon Evolution
                 */
                $this->setPokemonEvolution($userPokemon, $pokemon['evolutions']);

            }, 2);

            return response()->json([
                'title'   => 'Success!',
                'message' => 'You have obtained a new pokémon!',
            ], 200);

        } catch (\Throwable $th) {

            return response()->json([
                'title'   => 'Error!',
                'message' => 'The pokémon cannot be captured',
                // 'error'   => $th->errorInfo[2],
            ], 500);
        }
    }

    public function update($id)
    {
        try {

            DB::transaction(function () use ($id) {

                /**
                 * Evolve
                 */
                $userPokemon = UserPokemon::find($id);

                $userPokemon->evolution = 1;
                $userPokemon->save();

                $pokemonEvolution = $userPokemon->pokemonEvolution();

                $pokemonEvolution->evolution = 1;
                $pokemonEvolution->save();

            }, 2);

            return response()->json([
                'title'   => 'Success!',
                'message' => 'Your pokémon has evolved!',
            ], 200);

        } catch (\Throwable $th) {

            return response()->json([
                'title'   => 'Error!',
                'message' => 'Your pokémon has not been able to evolve',
            ], 500);
        }
    }

    public function destroy(UserPokemon $userPokemon)
    {
        try {

            DB::transaction(function () use ($userPokemon) {

                $userPokemon->status = 0;
                $userPokemon->save();

            }, 2);

            $title   = 'Freed!';
            $message = 'The pokémon has been released!';

            return response()->json([
                'title'   => $title,
                'message' => $message,
            ], 200);

        } catch (\Throwable $th) {

            $title   = 'Unfreed!';
            $message = 'You cannot free the pokémon.';

            return response()->json([
                'title'   => $title,
                'message' => $message,
            ], 500);
        }
    }

    public function validateForm($request)
    {
        $rules = [
            'pokemon_id' => 'required',
        ];

        $messages = [
            'pokemon_id.required' => 'The pokémon is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        return $validator;
    }

}
